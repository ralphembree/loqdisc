# loqdisc
My friends moved away from SO chat to go to Discord, so I took my bot (https://github.com/ralphembree/Loquitor) with me when I followed.  Most of the commands are the same, but they may take a slightly different form. I took out the kaomoji substitutions to keep with the [Best practices for Discord bots](https://github.com/meew0/discord-bot-best-practices). Other than that, you should find everything you need to know about the commands at my [other repository](https://github.com/ralphembree/Loquitor).

Of course, a new chat system requires a new API.  Because of this, the contribution details for Loquitor will not work for loqdisc. Some things are the same, but a lot of things are different. Fortunately, discord.py has its own documentation.  The `loqdisc` package contains everything, unlike Loquitor.  The `__main__.py` is what gets executed when `loqdisc` is given from the command-line.  The `__init__.py` is just empty.  Other than that, they are all files to add new commands. Because of this, the package doesn't even need to be installed.  If you want, you could just download and run without worrying about where the folder goes.  Of course, there are also dependences.  Right now, those dependencies are:

* BingTranslator: for the `translate` command found in translate.py
* pypygo: for the `instant` command found in search.py
* feedparser: for the `wotd` command found in wotd.py

If you remove those commands, the dependencies can be done away with. I might take out the `instant` command at some point because DuckDuckGo's instant answer API is quite limited, and I'll probably take away the `translate` command because Bing is changing their system to something that will require a new module that I don't have time to write myself and probably doesn't exist elsewhere.

When a script file is imported, the `bot` variable is already defined so that a command can be added in this simple fashion:

    @bot.command(description="Add numbers")
    async def amazing(*numbers: int):
        await bot.say(str(sum(numbers)))

Just put that function either in one of the existing files or in a new one, and it is a new command. The `async` and `await` are very important.  Without them, your code simply won't work.  This function will give the following help message:

    Add numbers
    
    >>amazing [numbers...]

There is more you can do with `bot.command()`.  You can find what is allowed by running `pydoc3 discord.ext.commands.core`.  If you're like me and you're default Python 3 version is 3.4, you'll need to use pydoc3.5 or pydoc3.6.

I also once had a problem because I had an instance method for a command and `self` was included as an expected argument. To save you from looking through Discord's documentation, just don't use the decorator function.  Instead, do it manually like this:

    from discord.ext.commands.core import Command

    class MyClass:
        def __init__(self, ...):
            ...
            command = Command('mycommand', self.mycommand_meth, description='Something amazing!')
            bot.add_command(command)

        def mycommand_meth(self, ...):
            ...
