import argparse
import builtins
from getpass import getpass
from importlib import import_module
import os
import sys
from warnings import warn

from discord.ext import commands

def get_from_options():
    parser = argparse.ArgumentParser(
        description="Chatbot for Discord",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument("-t", "--token", help="Token with which to authenticate")
    parser.add_argument("-c", "--config-dir", help="Alternate folder to search for configuration files")
    parser.add_argument("--no-input", help="Do not ask for input", action="store_true")
    return parser.parse_args()
# bot needs to be defined in the global namespace for each imported module.
builtins.bot = commands.Bot(
    command_prefix=commands.when_mentioned_or('>>'), description='Fun bot.'
)


config = vars(get_from_options())
envs = {
    'token': 'LOQ_TOKEN',
}
keys = config.keys()
no_input = config['no_input']

bot.no_input = no_input
bot.config_dir = config['config_dir'] or os.path.expanduser("~/.loqdisc")

scripts = {}
for filename in os.listdir(__package__):
    if filename.startswith(".") or filename.startswith("_") or not filename.endswith(".py"):
        continue
    mod_name = os.path.splitext(filename)[0]
    try:
        module = import_module(__package__ + '.' + mod_name)
        scripts[mod_name] = module
    except Exception as e:
        warn(repr(e))

if config['token'] is None:
    if no_input:
        sys.exit("Could not find token.")
    else:
        config['token'] = getpass('Token: ')

if not bot.no_input:
    print(
"""Welcome to Loquitor! The bot runs in the background, so you don't need to
do anything here. Just keep this terminal open if you want Loquitor to cont-
inue. If you want to stop Loquitor, hit the KeyboardInterrupt (Ctrl-C). If
you want Loquitor to continue, but you don't want the terminal to stay open,
stop Loquitor and re-run with & at the end. That will start Loquitor in the
background. If you then ever want to stop Loquitor, you'll need to do it
manually with the kill or pkill bash commands."""
    )

bot.run(config['token'])
