from random import choice
from re import findall
from string import punctuation

from urllib.request import urlopen
from bs4 import BeautifulSoup
import discord


GREETINGS = (
    "Hey, {}. How are you?", "Hello, {}.", "Heyo there, {}!", "{}: Hello.",
    "Howdy, {}.",
)


@bot.command(description="Repeat text.")
async def say(*words: str):
    query = " ".join(words)
    # \u200b is an invisible character to prevent recursion when the
    # query starts with the command prompt.
    query = "\u200b" + query.replace("@", "@\u200b")

    await bot.say(query)


async def greet_single(user):
    username = user.display_name
    additional = "I am a bot. For a list of my commands, type `>>help`."
    greeting = " ".join([choice(GREETINGS), additional])
    await bot.say(greeting.format(username))


@bot.command(description="Given a space-separated list of usernames, "\
"greet those users.")
async def greet(*users: discord.Member):
    for user in users:
        # Assuming some non-programmer thinks it should be written in English.
        await greet_single(user)
