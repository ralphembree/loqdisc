import os

from setuptools import setup

config_path = os.path.expanduser("~/.loqdisc")

setup(name='loqdisc',
      version='1.0',
      description='Chatbot for Discord',
      author='Ralph Embree',
      author_email='ralph.embree@brominator.org',
      url='https://github.com/ralphembree/loqdisc',
      packages=['loqdisc'],
      install_requires=['BingTranslator', 'feedparser', 'discord.py'],
      scripts=['bin/loqdisc'],
)
